﻿using Android.OS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Android.Util;
using NaKreche2.ViewModel;
using System.Collections.ObjectModel;
using System.IO;

namespace NaKreche2
{
    public partial class MainPage : CarouselPage
    {

    
        ViewModel.PersonViewModel PersonVM;
      
        public MainPage()
        {
         
            PersonVM = new ViewModel.PersonViewModel();
            // PersonVM.CreatePersonsList();
           
            //BindingContext = PersonVM;
            //Log.Info("MY INFO:", PersonVM.Persons.Count.ToString());
            InitializeComponent();
            BindingContext = PersonVM;
            //RandomizePicture("girl", ".png", 2,2);
            PersonsViewList.ItemsSource = PersonVM.Persons;
            UnDebtPersonsViewList.ItemsSource = PersonVM.UnDebtpersons;
            loadPersonsLists();

        }

        // internal PersonViewModel PersonVM { get => personVM; set => personVM = value; }

        private void AddPersonButtonClick(object sender, EventArgs e)
        {
            EntryPersonName.Focus();
            PersonsViewList.Opacity = 0.3;
            Button personAddButton = (Button)sender;
            EntryPersonName.IsVisible = true;
            ChangeButtonsState(false);
            btnCloseEntries.IsVisible = true;
        }

        void ChangeButtonsState(bool areEnabled) {
            btn1.IsEnabled = areEnabled;
            btn2.IsEnabled = areEnabled;
            btn3.IsEnabled = areEnabled;
            btn4.IsEnabled = areEnabled;
        }

        public async void NewPersonNameCompletedAsync(object sender, EventArgs e)
        {

            PersonsViewList.IsEnabled = false;
            PersonsViewList.Opacity = 0.3;
            EntryPersonName.IsVisible = false;
            btnCloseEntries.IsVisible = false;
            await Task.Delay(500);
            btnCloseEntries.IsVisible = true;
            EntryPersonDebt.IsVisible = true;
            EntryPersonDebt.Focus();
            EntryPersonDebt.Text = "";
            savePersonsLists();
           
        }

        private void PersonDebtCompleted(object sender, EventArgs e)
        {
          // if (EntryPersonName.IsVisible ==false && EntryPersonDebt.Text != "")
           // {
                if (EntryPersonDebt.Text != "" && EntryPersonName.Text != "")
                {

                if (Int32.TryParse(EntryPersonDebt.Text, out int x))
                {
                    x = x+0;
                }

                PersonVM.AddPerson(EntryPersonName.Text, x, DateTime.Today, PersonVM.Persons.Count);
                    EntryPersonDebt.IsVisible = false;
                    EntryPersonName.IsVisible = false;
                  
                }
            else if(EntryPersonDebt.Text != "" && EntryPersonName.Text == "")
                {

             
                Entry personDebt = (Entry)sender;
                personDebt.IsVisible = false;
                Model.Person person = (Model.Person)PersonsViewList.SelectedItem;
                int Y;
                if (Int32.TryParse(personDebt.Text, out Y))
                {
                    PersonVM.Persons[person.Id].Debt = Y;
                }else
                {
                    PersonVM.Persons[person.Id].Debt = PersonVM.Persons[person.Id].Debt;
                }
                PersonsViewList.IsEnabled = true;
                PersonsViewList.Opacity = 1;
           // }
            }
            PersonsViewList.IsEnabled = true;
            PersonsViewList.Opacity = 1;
            EntryPersonName.Text = "";
            EntryPersonDebt.IsVisible = false;
            savePersonsLists();
            ChangeButtonsState(true);
            btnCloseEntries.IsVisible = false;
        }

        private void ChangeDebtButtonClick(object sender, EventArgs e)
        {
            var clickedBtn = (Button)sender;
            var persons = PersonsViewList.ItemsSource;
            var person = (from oPerson in PersonVM.Persons
                        where oPerson.Id == (int)clickedBtn.CommandParameter
                        select oPerson).FirstOrDefault();
            PersonsViewList.SelectedItem = person;
            EntryPersonDebt.Text = "";
           
            EntryPersonDebt.IsVisible = true;
            EntryPersonDebt.Focus();
            PersonsViewList.Opacity = 0.3;
            PersonsViewList.IsEnabled=false;
            savePersonsLists();
            btnCloseEntries.IsVisible = true;

        }

        private void RemoveAllPersons(object sender, EventArgs e)
        {
            if (PersonVM.Persons!=null)
            {

       
            PersonsViewList.IsEnabled = true;

            foreach (Model.Person person in PersonVM.Persons)
            {
                PersonVM.UnDebtpersons.Add(person);
            }

            PersonVM.Persons.Clear();
            EntryPersonDebt.IsVisible = false;
            EntryPersonName.IsVisible = false;
            savePersonsLists();
            btnCloseEntries.IsVisible = false;
            ChangeButtonsState(true);
            }
        }

        void RandomizePicture(string picGroupName, string picExtension,  int minNo, int maxNo) {
            Random rand = new Random();
            backgroundImage.Source = picGroupName + rand.Next(minNo, maxNo).ToString() + picExtension;
            
        }

        private void DebtList_Disappearing(object sender, EventArgs e)
        {
            ChangeButtonsState(true);
            EntryPersonDebt.IsVisible = false;
            EntryPersonName.IsVisible = false;
            btnCloseEntries.IsVisible = false;
            //RandomizePicture("girl", ".png", 2, 2);
        }

        private void RemovePerson(object sender, EventArgs e)
        {
            if (PersonsViewList.SelectedItem!=null)
            {
            Model.Person person = (Model.Person) PersonsViewList.SelectedItem;
            PersonVM.AddUnDebtPerson(person.MainName, person.Debt, person.StartDebtDate, DateTime.Today);
            PersonVM.Persons.Remove(person);
            savePersonsLists();
            }
        }
        
        private void ClearHistory(object sender, EventArgs e)
        {
           
            PersonVM.UnDebtpersons.Clear();
            savePersonsLists();
        }

        private void savePersonsLists()
        {
            // ObservableCollection<Model.Person> tmpPersons = PersonVM.Persons;
            SaveLoad.SaveToJSON(PersonVM.Persons, "listPersons.txt");
            SaveLoad.SaveToJSON(PersonVM.UnDebtpersons, "listPersonsUnDebt.txt");
        }

        private void loadPersonsLists()
        {
           var documentsPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            var filePath = Path.Combine(documentsPath, "listPersons.txt");
            if (File.Exists(filePath))
            {

           
            ObservableCollection<Model.Person> tmpPersons = (ObservableCollection<Model.Person>)SaveLoad.LoadFromJSON("listPersons.txt");
            PersonVM.Persons = new ObservableCollection<Model.Person>(tmpPersons);
            BindingContext = PersonVM;
            PersonsViewList.ItemsSource = PersonVM.Persons;
            UnDebtPersonsViewList.ItemsSource = PersonVM.UnDebtpersons;
            }
            else
            {
                PersonVM.Persons = new ObservableCollection<Model.Person>();
                BindingContext = PersonVM;
                PersonsViewList.ItemsSource = PersonVM.Persons;
                UnDebtPersonsViewList.ItemsSource = PersonVM.UnDebtpersons;
         
            }


            var documentsPath2 = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            var filePath2 = Path.Combine(documentsPath, "listPersonsUnDebt.txt");
            if (File.Exists(filePath2))
            {
                ObservableCollection<Model.Person> tmpPersonsUnDebt = (ObservableCollection<Model.Person>)SaveLoad.LoadFromJSON("listPersonsUnDebt.txt");
                PersonVM.UnDebtpersons = new ObservableCollection<Model.Person>(tmpPersonsUnDebt);
                BindingContext = PersonVM;
                PersonsViewList.ItemsSource = PersonVM.Persons;
                UnDebtPersonsViewList.ItemsSource = PersonVM.UnDebtpersons;
            }
            else
            {
                PersonVM.UnDebtpersons = new ObservableCollection<Model.Person>();
                BindingContext = PersonVM;
                PersonsViewList.ItemsSource = PersonVM.Persons;
                UnDebtPersonsViewList.ItemsSource = PersonVM.UnDebtpersons;
           
            }
            savePersonsLists();
        }


        private void saveDebugClick(object sender, EventArgs e)
        {
           // ObservableCollection<Model.Person> tmpPersons = PersonVM.Persons;
            SaveLoad.SaveToJSON(PersonVM.Persons, "listPersons.txt");
            SaveLoad.SaveToJSON(PersonVM.UnDebtpersons, "listPersonsUnDebt.txt");
        }
                private void loadDebugClick(object sender, EventArgs e)
        {
            ObservableCollection<Model.Person> tmpPersons = (ObservableCollection<Model.Person>)SaveLoad.LoadFromJSON("listPersons.txt");
            PersonVM.Persons = new ObservableCollection<Model.Person>(tmpPersons);
            BindingContext = PersonVM;
            PersonsViewList.ItemsSource = PersonVM.Persons;
            UnDebtPersonsViewList.ItemsSource = PersonVM.UnDebtpersons;

            ObservableCollection<Model.Person> tmpPersonsUnDebt = (ObservableCollection<Model.Person>)SaveLoad.LoadFromJSON("listPersonsUnDebt.txt");
            PersonVM.UnDebtpersons = new ObservableCollection<Model.Person>(tmpPersonsUnDebt);
            BindingContext = PersonVM;
            PersonsViewList.ItemsSource = PersonVM.Persons;
            UnDebtPersonsViewList.ItemsSource = PersonVM.UnDebtpersons;
        }

        private void CloseEntries(object sender, EventArgs e)
        {
            EntryPersonDebt.IsVisible = false;
            EntryPersonName.IsVisible = false;
            btnCloseEntries.IsVisible = false;
            ChangeButtonsState(true);
            PersonsViewList.IsEnabled = true;
            PersonsViewList.Opacity = 1;
        }

        private void ValidateName(object sender, TextChangedEventArgs e)
        {
            var entry = (Entry)sender;

            if (entry.Text.Length >18)
            {
                string entryText = entry.Text;
                entry.TextChanged -= ValidateName;
                entry.Text = e.OldTextValue;
                entry.TextChanged += ValidateName;
            }

        }

        private void ValidateInt(object sender, TextChangedEventArgs e)
        {
            var entry = (Entry)sender;
            if (entry.Text.Length > 9)
            {
                string entryText = entry.Text;
                entry.TextChanged -= ValidateName;
                entry.Text = e.OldTextValue;
                entry.TextChanged += ValidateName;
            }
            if (entry.Text.Length == 0)
            {
                string entryText = entry.Text;
                entry.TextChanged -= ValidateName;
                entry.Text = "";
                entry.TextChanged += ValidateName;
            }
        }
    }
}
