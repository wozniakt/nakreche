﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;

namespace NaKreche2.ViewModel
{
    
    class PersonViewModel : INotifyPropertyChanged
        
    {
     
        private ObservableCollection<Model.Person> _persons;
        public ObservableCollection<Model.Person> Persons
        {
            get
            {

                //SaveLoad.SaveToJSON(_persons);
                
                    //saveLoad.SaveToJSON(Persons);
              

                return _persons; }

            set
            {
                _persons = value;
              
                OnPropertyChanged("Persons");
            }
        }


        private ObservableCollection<Model.Person> _unDebtpersons;
        public ObservableCollection<Model.Person> UnDebtpersons
        {
            get { return _unDebtpersons; }

            set
            {
                _unDebtpersons = value;
                OnPropertyChanged("UnDebtpersons");
            }
        }
        //public List<Model.Person> Persons;
        public PersonViewModel()
        {
            UnDebtpersons = new ObservableCollection<Model.Person>();
            Persons = new ObservableCollection<Model.Person>();
            Persons.Add(new Model.Person() { MainName = "Alojzy" , Debt = 1000, StartDebtDate = DateTime.Today, Id = Persons.Count });

        }
        public void AddPerson(string Name, int debt, DateTime DebtDate, int Id) {
            Persons.Add(new Model.Person() { MainName = Name, Debt=debt, StartDebtDate=DebtDate, Id=Persons.Count });
        }

        public void AddUnDebtPerson(string name, int debt, DateTime startDebtDate,DateTime endDebtTime)
        {
            UnDebtpersons.Add(new Model.Person() { MainName = name, Debt = debt, StartDebtDate = startDebtDate, EndDebtDate= endDebtTime, Id = UnDebtpersons.Count });
        }

        public string LoadText() {
            string textFromFile=   SaveLoad.LoadText("listPersons.txt");
            return textFromFile;
        }

        private void OnPropertyChanged()
        {
            throw new NotImplementedException();
        }

        public event PropertyChangedEventHandler PropertyChanged;


        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}