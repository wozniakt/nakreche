﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Text;

namespace NaKreche2.ViewModel
{
    static class SaveLoad
    {
       
            public static void SaveText(string filename, string text)
    {
        var documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
        var filePath = Path.Combine(documentsPath, filename);
        System.IO.File.WriteAllText(filePath, text);
    }
    public static string LoadText(string filename)
    {
        var documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
        var filePath = Path.Combine(documentsPath, filename);
        return System.IO.File.ReadAllText(filePath);
    }

    public static void SaveToJSON(ObservableCollection<Model.Person> listPersons, string fileToSave) {

        string json = JsonConvert.SerializeObject(listPersons);
        string path = Environment.GetFolderPath(Environment.SpecialFolder.Personal);

        string filePath = Path.Combine(path, fileToSave);
           

        using (var file = File.Open(filePath, FileMode.Create, FileAccess.Write))
        using (var strm = new StreamWriter(file))
        {
            strm.Write(json);
        }
       
    }

        public static ObservableCollection<Model.Person> LoadFromJSON(string fileToLoad)
        {
            ObservableCollection<Model.Person> loadedList = new ObservableCollection<Model.Person>();
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            string filePath = Path.Combine(path, fileToLoad);

            using (StreamReader sr = new StreamReader(filePath))
            {
                using (JsonReader reader = new JsonTextReader(sr))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    loadedList = serializer.Deserialize<ObservableCollection<Model.Person>>(reader);
                }
                //'reader' will be disposed by this point
            }
            //'sr' will be disposed by this point
           
            return loadedList;
        }

    }
}
 

