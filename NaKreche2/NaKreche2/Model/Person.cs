﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;

namespace NaKreche2.Model
{
    class Person : INotifyPropertyChanged
    {
        private string _mainName;
        public string MainName
        {
            get { return _mainName; }
            set
            {
                _mainName = value;
                OnPropertyChanged("MainName");
            }
        }
        private int _debt;
        public int Debt
        {
            get { return _debt; }
            set
            {
                _debt = value;
                OnPropertyChanged("Debt");
            }
        }
        private DateTime _startDebtDate;
        public DateTime StartDebtDate
        {
            get {
               
                return _startDebtDate; }
            set
            {
                _startDebtDate = value;
                OnPropertyChanged("StartDebtDate");
            }
        }


        private DateTime _endDebtDate;
        public DateTime EndDebtDate
        {
            get
            {

                return _endDebtDate;
            }
            set
            {
                _endDebtDate = value;
                OnPropertyChanged("StartDebtDate");
            }
        }



        private int _id;
        public int Id
        {
            get { return _id; }
            set
            {
                _id = value;
                OnPropertyChanged("Id");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
